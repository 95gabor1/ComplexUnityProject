﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public Animator DoorAnimator;
	public float Speed = 2.0f;
	public Camera Camera;

	private NavMeshAgent NavMeshAgent;
	private Vector3 CameraRelativeOffset;

	void Start()
	{
		NavMeshAgent = GetComponent<NavMeshAgent>();

		CameraRelativeOffset =
			Camera.transform.position -
			transform.position;
	}
	
	void Update()
	{
		GetComponent<Animator>().SetFloat(
			"Speed",
			NavMeshAgent.velocity.magnitude
		);

		Camera.transform.position =
			transform.position + CameraRelativeOffset;

		if (Input.GetMouseButtonDown (0))
		{
			Ray ray = Camera.main.ScreenPointToRay(
				Input.mousePosition
			);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, 100))
			{
				NavMeshAgent.SetDestination(hit.point);
			}
		}
	}
}
